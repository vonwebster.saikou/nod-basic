// Exponentiation `**`

// first will be the base and the second is the exponent
let base = 2
let exponent = 4
let a = base ** exponent // 2 * 2 * 2 * 2 == 16

console.log(a)