// Javascript coding standards 
// keep your code clean

// 1. Use === instead of ==

if (2 === 2) console.log('Yes');


// 2. Never use var, use let

let name = 'Arcy';

// 3. Use const instead of let
// helps readability

const taxPercent = 10;

// 4. Always put semicolons

let amount = 10.00;

// 5. Naming conventions

// -- `let` should be camelCase
// -- `const` at the top of file use upper case snake case
// `MY_CONST` if not use camelCase
// -- 'class' PascalCasing `MyClass`
// -- functions should be camelCase `myFunction`

const VAT_PERCENT = 12;
let petName = 'Aigoo';
class MyClass {
    myFunction() {
        const vatPercent = 12;
    }
}

// 6. Use template literals when contacting strings

let info = `${name} have pet named ${petName}`;

// 7. Use arrow function where possible

const vatAmount = (amount) => { return amount * VAT_PERCENT };

// 8. Always use curly braces arount control structures

if (2 === 2) {
    console.log('Yes');
}

// 9. Curly braces starts on the same line with the space

if (2 === 2) {
    console.log('Yes');
}

// 10. Reduce nested if

// 11. Max line length for Files and functions

// -- `files` max of 80 lines
// -- `functions` max of 15 lines

// 12. lowercase file names
// `MyFile.js` should be `myFile.js`

// 13. Switch statements use `break` and have `default`
function addDiscountPercent(discount) {

}
let myNumber = 1;
switch (myNumber)
{
  case 10: 
    addDiscountPercent(0);
    break;
  case 20: 
    addDiscountPercent(2);
    break;
  case 30:
    addDiscountPercent(3);
    break;
  default: 
    addDiscountPercent(0);
    break;
}

// 14. use `named` exports
// do not use `default` exports. Importing modules must five a name to these
// values, which can lead to inconsistencies in naming across modules
// `export class MyClass {`

// 15. Do not use wildcard imports
// Make sure you have a single default export

// import Foo from './Foo';

// 16. Only use one variable per declaration

let a = 1;
let b = 2;
