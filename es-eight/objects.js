// Object.keys, Object.values & Object.entries

// converts to iterable objects(array) from the object
// then array can used loops, filter, map, reduce

const objSales = { 
    Aigoo : 50, 
    Cornietta : 90, 
    Jaddy : 80, 
    Jaccy : 30, 
    Hamtaro : 40
}

console.log('Object Keys')
console.log(Object.keys(objSales))
console.log('Object Values')
console.log(Object.values(objSales))
console.log('Array of Object')
console.log(Object.entries(objSales))