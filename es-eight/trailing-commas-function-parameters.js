 function clownPuppiesEverywhere(
 param1,
 param2, // Next parameter that's added only has to add a new line, not modify this line
) { 
    console.log(param1, param2)
}

clownPuppiesEverywhere(
 'foo',
 'bar', // Next parameter that's added only has to add a new line, not modify this line
);

/**
 Note that this proposal is exclusively about 
 grammar and makes no changes to semantics, therefore
 the presence of a trailing comma has no effect on things
 like <<function>>.length.
 */