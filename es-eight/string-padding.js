// padStart() & padEnd()

const padAtStart = (inputStr, length, char) => {
    let start = ''
    for(let i = 0; i < length; i++) {
        start += char
    }
    console.log(start)
    const retVal = start + inputStr
    return retVal.slice(-length)
}
console.log(padAtStart('Arcyle', 10, '1'))

// console.log('Name'.padEnd(20, " ") +': Arcylen Gutierrez')
// console.log('Phone Number'.padEnd(20, " ")+': 555-555-1234')

// Add char to end and start of the string
// arguments are `number of char` and the `char`

// formatting money
const peso = 20
const cents = 1
const formattedCents = cents.toString().padStart(2, '0')
console.log(`P${peso}.${formattedCents}`)

// formatted dates
const year = 1995
const month = 10
const formattedMonth = month.toString().padStart(2, '0')
console.log(`${formattedMonth}/${year}`)

// formatting timer

const seconds = (5).toString().padStart(2, 0)
const ms = (1).toString().padStart(2, 0)
console.log(`${seconds}:${ms}`)