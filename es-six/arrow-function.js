// Normal Function
var normalFunction = function (name) {
    return 'Hi '+ name +', this is a normal fucntion';    
}
// -- Set `this` to a window object

/**Medium */
// -- Arrow fucntion don't have `argurment binding` (are the real values passed to (and received by) the function.

// Sample of arguments binding in normal functions
let myFunc = {  
    showArgs(){ 
     console.log(arguments); 
    } 
}; 
// myFunc.showArgs(1, 2, 3, 4);



/* Arrow Function */
var arrowFunction = (name) => {
    return 'Hi '+ name +', this is a arrow function';
}

// console.log(normalFunction('Normal'));
// console.log(arrowFunction('Arrow'));


const dragonEvents = [
    { type: 'attack', value: 12, target: 'player-dorkman'},
    { type: 'yawn', value: 40},
    { type: 'eat', target: 'horse'},
    { type: 'attack', value: 23, target: 'player-fluffykins'},
    { type: 'attack', value: 12, target: 'player-dorkman'},
];

const reduceTotal = (prev, value) => (prev || 0) + value;
const totalDamageOnDorkman = dragonEvents.filter(e => e.type === 'attack' && e.target === 'player-dorkman')
                                         .map(e => e.value)
                                         .reduce(reduceTotal);

// console.log('Result\n', totalDamageOnDorkman);

/**MPJ - FunFun Function */
// -- shorter function syntax (kind of)
// -- `this` behaves differently
// -- arrow function can use in a new way
// -- Can do function that are Small, Inline, Single purpose
// -- Cannot be named, when you uses function inline you 
// normally don't need a function to be name
// -- Ommit/remove the parenthesis if you just have one parameter

/**Medium */
// -- arrow functions do not have their own `this`
let me = { 
    name: "Ashutosh Verma", 
    thisInArrow:() => { 
        console.log("My name is " + this.name); // no 'this' binding here 
    }, 
    thisInRegular() { 
        console.log("My name is " + this.name); // 'this' binding works here 
    } 
};
// me.thisInArrow(); // Will return undefined
// me.thisInRegular();

// -- arrow function can never be invoked or instantiate with the `new` keyword
let add = function (x, y) {
    console.log(x + y)
};
// new add(2,3);

// -- arrow function can never have duplicate parameters wether in strict or non-strict mode
let addDuplicateParam = (x, x) => {
    console.log(x + x);
} // Will returned error that duplicate parameter is not allowed
// addDuplicateParam(3, 7);

/** END of Arrow Function */
