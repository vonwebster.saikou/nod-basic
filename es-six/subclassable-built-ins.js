// Subclassable built-ins
// In ES6, built-ins like Array, Date and DOM Elements can be subclassed.

// Array
class MyArray extends Array {
    constructor(...arr) {
        super(...arr)
    }
}
// Classes alone will return an object but
// when extended to Array will return an array
const petArray = new MyArray()
petArray[0] = "Aigoo";
petArray[1] = "Cornietta";
console.log(petArray)


// Date
class MyDate extends Date {
    constructor(...date) {
        super(...date)
    }
}
const petDate = new MyDate()
console.log(petDate)

// 	they are added to instances	by the built-in constructors.
// You can have unique identifier to your Class with Date, Array properties

/**
 Array
    The custom internal instance method [[DefineOwnProperty]] intercepts
    properties being set. It ensures that the length property works correctly,
    by keeping length up-to-date when array elements are added and by
    removing excess elements when length is made smaller.
Date
    The internal instance property [[PrimitiveValue]] stores the time
    represented by a date instance (as the number of milliseconds since
    1 January 1970 00:00:00 UTC).
Function
    The internal instance property [[Call]] (the code to execute 
    when an instance is called) and possibly others.
RegExp
    The internal instance property [[Match]], plus two
    noninternal instance properties. From the ECMAScript specification:

    The value of the [[Match]] internal property is an implementation
    dependent representation of the Pattern of the RegExp object.

The only built-in constructors that don’t have internal properties are
Error and Object.
 */