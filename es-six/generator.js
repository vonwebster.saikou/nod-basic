// GENERATORS
// syntactic sugar to create iterators

// const dragonArmy = {
//     [Symbol.iterator]: function* () {
//         while(true) {
//             const enoughDragonSpawned = Math.random > 0.75
//             if (enoughDragonSpawned) return
//             yield 'sample '
//         }
//     }
// }

function* someDragons() {
    // while(true) {
    //     const enoughDragonSpawned = Math.random > 0.75
    //     if (enoughDragonSpawned) return
        yield 'sample ' // will iterate
    // }
}

const iterator = someDragons()
console.log(iterator.next())
console.log(iterator.next())
console.log(iterator.next())
// console.log(dragonArmy)
// for (const dragon of dragonArmy) {
    // console.log(dragon)
// }

// Pausable function
// Calling a generator does not mean to execute the function
// it needed first to execute the first iterator,
// iterator is like a remote to generator 

// other sample
const fibonacci = {
    [Symbol.iterator]: function*() {
      let pre = 0, cur = 1;
      for (;;) {
        let temp = pre;
        pre = cur;
        cur += temp;
        yield cur;
      }
    }
  }
  
//   for (const n of fibonacci) {
//     // truncate the sequence at 1000
//     if (n > 1000)
//       break;
//     console.log(n);
//   }*