// Learn ReGex
// way to search of the string or text [Find and replace]
// https://regexr.com/
 
// to be continue




// -------------------------
// same as ES5.1
"𠮷".length == 2

// new RegExp behaviour, opt-in ‘u’
"𠮷".match(/./u)[0].length == 2

// new form
"\u{20BB7}"=="𠮷"=="\uD842\uDFB7"

// new String ops
"𠮷".codePointAt(0) == 0x20BB7

// for-of iterates code points
for(var c of "𠮷") {
  console.log(c);
}

const regex1 = new RegExp('\u{61}');
const regex2 = new RegExp('\u{61}', 'u');

console.log(regex1.unicode);
// expected output: false

console.log(regex2.unicode);
// expected output: true

console.log(regex1.source);
// expected output: "a"

console.log(regex2.source);
// expected output: "a"
