// VAR
// weakness of using allows you to redeclare the variable
var pet = 'aigoo'
var pet = 'cornietta'

// LET 
// use to declare a variable, array or an object
// same use for var but it can't allow the variable to be redeclare
let petLet = 'aigoo'
// let petLet = 'cornietta' // already been declared

console.log(petLet)


// CONSTANT
// cannot be redeclare and redefined
const number = 42;

try {
  number = 99;
} catch (err) {
  console.log(err);
  // expected output: TypeError: invalid assignment to const `number'
  // Note - error messages will vary depending on browser
}

console.log(number);
// expected output: 42

// Try in Array
// can add an array inside the const but can't be changed
const arr = [1, 3, 4];
arr.push(5)
console.log(arr);