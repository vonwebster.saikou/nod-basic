// script tag add `type='module' defer [for loading]`
// Same name function from the imported
import User, { printName, printAge } from './user.js'

const user = new User('Arcy', 24)
console.log(user)
printName(user)
printAge(user)