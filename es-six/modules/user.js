export default class User {
    constructor(name, age) {
        this.name = name
        this.age = age
    }
}

export function printName(user) {
    console.log(`The user name is ${user.name}`);
}
export function printAge(user) {
    console.log(`The user age is ${user.age}`);
}

// Export in the end of file
// export default User
// Exporting functions
// export { printName, printAge }