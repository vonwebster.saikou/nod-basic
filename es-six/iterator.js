// Iterator
// for..of uses to iterate over array, sets, map
// WHAT

const dragons = [
    'cool dragon',
    'angry dragon',
    'nasty dragon',
]

// Symbols[feature - unique object] basically unique keys, use them 
// where we would nirmally have use like a weird 
// unique string to avoid conflict
const iterator = dragons[Symbol.iterator]()
// Object [Array Iterator] {}
// { [Iterator] }
console.log(iterator) 
// iterator happens to have next method
// manually calling next what the for...of job 
// extra interface
console.log(iterator.next())
console.log(iterator.next())
console.log(iterator.next())
console.log(iterator.next())

for (const dragon of dragons) {
    console.log(dragon)
}

// WHY
// string object in Javascript provides an iterator for the for..of to use

const dragonArmy = {
    [Symbol.iterator]: () => { // Create an iterface
        return { // return an object
            next: () => { // method next
                const enoughDragonSpawned = Math.random > 0.75
                if (!enoughDragonSpawned)
                    return {
                        value: 'sample',
                        done: false
                    }
                return { done: true }
            }
        }
    }
}

for (const dragon of dragonArmy) {
    console.log(dragon)
}
// iterators can also be asynchronous
// which allow us to have iterators that gradually fetch 
// data from an API for instance, on each loop 

// Generator are thin syntactic sugar over what we have done for today
// to generates creates iterators