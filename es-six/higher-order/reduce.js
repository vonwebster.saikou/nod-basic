var fs = require('fs')

const path = 'es-six/higher-order/data.txt'
let output = fs.readFileSync(path, 'utf8')
    .trim()
    .split('\n')
    .map(line => line.split('\t'))
    .reduce((customers, line) => {
        customers[line[0]] = customers[line[0]] || []
        customers[line[0]].push({
            name: line[1],
            price: line[2],
            quantity: line[3]
        })
        return customers
    }, {})

console.log(JSON.stringify(output, null, 2))