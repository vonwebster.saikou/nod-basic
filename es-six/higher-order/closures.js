// Functions are values
// functions are also closures 
// function model has access to variables outside the function
// greetMe function are closures
let name = 'Arcy'
function greetMe() {
    console.log(`Hello, ${name} !`)
}
name = 'Meow' // does not copy, but reads
// the variable hoisted to the function
greetMe()

// WHY
// -- When you have a task and its done it was available to you
// Closures makes that easy and readable

// Javascript permeates the entire language 

// Mozilla
// A closure is the combination of a function
// bundled together (enclosed) with references to its 
// surrounding state (the lexical environment). In other words,
// a closure gives you access to an outer function’s scope from
//  an inner function. In JavaScript, closures are created every time 
// a function is created, at function creation time.