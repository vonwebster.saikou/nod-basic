// PROXIES
// they allow you to handle override

// override setters, getters to do some kind of validation
let handler = {
    get: function(target, key) {
        return key in target ? target[$key] : 'target not exist'
    }
}

// Syntax
// target = refers to the object
// handler = explains how it can be different when you access the target
// new Proxy(target, handler)

let p = new Proxy({}, handler) // with proxy
// let p = {} // withou proxy
p.a = 1
p.b = undefined
console.log(p)
console.log(p.c)

// with the proxy it will allow you to make other things to happen
// instead of the normally happen when you were working with objects


// SET METHOD
let validation = {
    set: function(person, key, value) {
        if (key === 'age') {
            if (typeof value !== 'number' || Number.isNaN(value)) {
                console.log('Age must be a number')
            }
            if (value < 0) {
                console.log('Age must be a positive numbe')
            }
        }
        person[key] = value

        return true
    }
}

let person = new Proxy({}, validation)
person.age = 12
console.log(person)