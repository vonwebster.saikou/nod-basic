/** BIND and THIS */
let cat = { // Object
     sound: 'meow', // Property
     talk: function() { // Method
         console.log(this.sound);
     }
 }
 cat.talk();
 // -- when assigning a method to a variable, it's now a function
 // it will not find the `this` becuase its not binded to the function
 /**
   let talkFunction = function() {
         console.log(this.sound); // Will returned undefined because 
         // `this` is not existing on the function 
    };
  */
 // with the use of `bind` you can now use the property on the assigned
 // method to the function
 let talkFunction = cat.talk;
 let boundFunction = talkFunction.bind(cat);
 boundFunction(); // will return the talk function

 // Real life example in binding the object to the function 
 /**
    let button = document.getElementById('myNiceButton');
    button.addEventListener('click', cat.talk.bind(cat));
 */
 /** BIND and THIS */