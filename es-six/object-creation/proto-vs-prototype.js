// _proto__ VS prototype
// Are Equal and same

/** understood that _proto_ is the same to the prototype of the constructor function. I mean:
objFromConstructor.__proto === Constructor.prototype */

// WHAT
let cat = {
    breed: 'munchkin'
}

let myCat = {
    name: 'Aigoo'
}


// Prototype is not class based inheritance, 
// much simplier model that delegate to other object
// Object delegation to other object
Object.setPrototypeOf(myCat, cat);
console.log(myCat.__proto__)