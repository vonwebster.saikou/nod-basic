// Prototype 
// -- In OOP, inheritance with clasess
// but in Javascript we achieve inheritance using prototype
// -- becuase new version of Javascript there is class keyword
// prototype powerful inheritance model, simplier concepts
// class 

// function talk(sound) {
//     console.log(sound);
// }
// talk('meowwww');

// in ES6 when assigning function to a property or method
// with the same name we can simpl omit the assignment of the function
// Example
/**
 let animal = {
     talk: talk (We can remove the other talk) => talk
     // it will return the same thing
 }
*/

function talk() {
    console.log(this.sound);
}

let animal = {
    talk
}
let cat = { 
    sound: 'meow'
}
let dog = { 
    sound: 'woof'
}
let prarieDog = {
    howl: function() {
        console.log(this.sound.toUpperCase());
    }
}
// prototype of cat is the animal
// inheriting the property of the object
Object.setPrototypeOf(cat, animal);
cat.talk();
Object.setPrototypeOf(dog, animal);
dog.talk();
Object.setPrototypeOf(prarieDog, dog);
prarieDog.howl();