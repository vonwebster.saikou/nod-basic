// Default Parameters
function makeSounds(sound1, sound2 = 'wisss') {
    console.log('It could be ' + sound1 + ' and also '+ sound2)
}
console.log('Default parameters')
makeSounds('meow')

// REST Parameters
// prefix by `...`
// must be the last parameter, `the rest parameters`
// Also support array destructuring
function add(a, b, ...otherNums) {
    return a + b + otherNums.reduce((prev, value) => (prev || 0) + value)
}
const result = add(12, 423, 23, 12, 5)
console.log('Rest parameters')
console.log(result)

// SPREAD Parametes
// looks like a rest parameters
// allows expression to be expanded and places where
// multiple arguments, elements or variables are expected

console.log('Spread parameters')
// 1. Add elements of an existing array into a new array
let numToAdd = [ 5, 6, 7];
let nums = [ 1, 2, 3, 4, ...numToAdd, 8, 9, 10]
console.log(nums)

// 2. Pass elements of an array as arguments to a function
// Spreading the arguments into different variables
function addTwoNums(a, b) {
    return a + b
}
var arr = [1, 3]
var sumOfTwoNums = addTwoNums(...arr)
console.log(sumOfTwoNums)
// 3. Can mutate and concatenate an array
var arr2 = [...numToAdd,...arr]
console.log(arr)
console.log(arr2)

// OTHERS 
// DESCTRUCTURING REST SPREAD IN ARRAY
const alphabet = ['A', 'B', 'C', 'D', 'E', 'F']
const numbers = ['1', '2', '3', '4', '5', '6']

// desctrucure the alphabet array and assign it to the
// variable and use the rest operator to get the rest of the array
const [a, , c, ...rest] = alphabet

// You can also set default value to desctructure variable

// IN OBJECT
const petOne = {
    age: '1',
    animal: {
        species : 'cat',
        color : 'white'
    }
}
const petTwo = {
    name: 'Cornietta',
    age: '8',
    animal: {
        species : 'cat',
    }
}
// assigning petName variable to the key property name
// taking the name property from the petOne and
// map to the assign variable petName
const { name: petName = 'Aigoo', animal: { species, eyeColor = 'Yellow' } , ...restPetInfo} = petOne
console.log(petName)
console.log(species)
console.log(eyeColor)
console.log(restPetInfo)

const petThree = { ...petOne, ...petTwo }
console.log(petThree)