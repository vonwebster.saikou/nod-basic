// Tail calls

function factorial(x) {
    if ( x === 1 ) return 1
    console.log(`x = ${x }, factorial(x - 1) = ${factorial(x - 1)}`)
    return x * factorial(x - 1)
}
console.log(factorial(5))

function fact(n) {
    return tailFact(n, 1)
}
function tailFact(x, acc) {
    if (x <= 1) return acc
    console.log(`x - 1 = ${x - 1}, x * acc = ${x * acc}`)
    return tailFact(x - 1, x * acc) // tail Call

}
console.log(fact(5))

// How 
// -- no additional work
// -- optimized the space
// -- you won't get the stack overflow

// Require
// - strict mode, there are some properties you can't use
// - not a generator function
// - returned value of function is returned

// Downsides
// -- Debugging, no stack call