// Destructuring
// WHAT 
// allows you to break a part, stuff into variable

var animal = {
    species: 'cat',
    weight: 21,
    sound: 'meow'
}

var { species, sound } = animal
// convert into this
// var species = animal.species
// var sound = animal.sound
console.log('The '+ species + ' says ' + sound + '!')

// WHy
// EASY to deal with the `option` object

makeSound({ // function with object literals with few property
    species: 'dog',
    weight: 25,
    sound: 'woof'
})

/**
function makeSound(options) { 
    // var species = options.species || 'animal'
    // var sound = options.sound
    var { species, sound } = options
    species = species || 'animal'
    console.log('The '+ species + ' says ' + sound + '!')
}
 */

// you can do destructuring in the method signature
// and assign default value in it
function makeSound({species = 'animal', sound }) { 
    console.log('The '+ species + ' says ' + sound + '!')
}

// Note
// propeties with variable even they are nested several layers deep
// not limited desctructuring object but can also desctruturing array