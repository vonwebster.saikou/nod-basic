// Template string using ``

// Basic literal string creation
let newline = `In JavaScript '\n' is a line-feed.`

console.log(newline);

// Multiline strings
let multiLine = `In JavaScript this is
 not legal.`

console.log(multiLine);

// String interpolation
var name = "Bob", time = "today"
console.log(`Hello ${name}, how are you ${time}?`)
