"use strict"

/** SET */
let myArray = [11, 11, 22, 21, 32, 42]
let myset = new Set(myArray) // Will remove duplicate value on our array

myset.add(100)
myset.add('arcy')
myset.add({a: 1, b: 5}) // Add to the array
//myset.delete(32) // delete from the array
// myset.clear() // will clear all the set
myset.size // lenght of tthe set

myset.has(12) // will return boolean if the value exisiting in the array

// will show the `Set`
// console.log(myset)

// converting the `Set` to array using spread operator
const uniqueArray = [...myset];
// console.log(uniqueArray);

// `Set` unorder pool of unique elements
// no integer index to you use to access specific element of a set
// if myArray[3] will return `21`
// then myset[3] will return `undefined`

// `Set` is different from an array, not meant to replace the array entirely
// but to provide additional support type to complete what an array is missing
// to avoid saving duplicate data to our structure
/** END SET */


/** MAP */
let myMap = new Map([['name', 'Arcy'], ['surname', 'Gutierrez']])

// console.log(myMap) 

// holds key value pairs and remembers original insertion order keys
// You can use object as keys!!! { SUGOIIII }

// Javascripts objects support only or allows to have one key objects
// if we have multiple key objects it will only remembers the last key object 
// thats why we need to use Map

const myObect = {}
const a = {}
const b = {}

myObect[a] = 'a'
// it will only remember the last key object
myObect[b] = 'b'
// console.log(myObect)

let myObjectMap = new Map([[a, 'Arcy'], [b, 'Gutierrez']])

// set Method - will add an object to our map
myObjectMap.set({}, 24)
// delete Method - it will delete an object using keys
// clear Method - will return empty Map
// has Method - same with set, search for keys
// get Method - will get the value of the map using keys(Object, string and etc)
// size method
// console.log(myObjectMap)

// ?? What if the the keys are object, how can you target that?
/** END MAP */

/** WEAKSET */
let weakSet = new WeakSet()
let mySetWS = new Set()
// weakness
// -- You can't get size, enumerate (loop? YESSS) - Weakly referenced
// has add, delete

let obj1 = {}
let obj2 = {}

mySetWS.add(obj1)
mySetWS.add(obj2)

weakSet.add(obj1)
weakSet.add(obj2)

obj1 = null
obj2 = null

// even though we re-assign value to our object that we set to the SET
// it will retain to the SET, unless you directly 
// unset/deleted/update to the SET

console.log(mySetWS)
console.log(weakSet)

// console.log(weakSet)
/** END WEAKSET */

/** WEAKMAP */
let myMapWP = new Map()
let weakMap = new WeakMap()

// weakness
// -- You can't get size, enumerate (loop? YESSS) - Weakly referenced
// but the positive that they get garbage collected 
// immediately with no other efforts

let catMapObj = {'name' : 'Aigoo'}
let catWeakmapObj = {'name' : 'Cornietta'}

myMapWP.set(catMapObj, `Hi ${catMapObj.name}`)
weakMap.set(catWeakmapObj, `Hi ${catWeakmapObj.name}`)

// console.log(myMapWP.get(catMapObj))
// console.log(weakMap.get(catWeakmapObj))

catMapObj = null
catWeakmapObj = null

// even though we re-assign value to our object that we set to the map
// it will retain to the map, unless you directly 
// unset/deleted/update to the map
// console.log(myMapWP) 

// Will get undefined because it our object was assign as null
// console.log(weakMap) // Items unknown
// console.log(weakMap.get(catWeakmapObj)) // undefined
// Weakmap is weak referencing
/** END WEAKMAP */