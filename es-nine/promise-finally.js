function getPetList() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('Pet list here')
            // reject('Error loading list')
        }, 2000)
    })
}

getPetList()
    .then(response => console.log(response)) // Resolve
    .catch(error => console.log(`Error! ${error}`)) // Reject
    .finally(() => console.log('Finally') ) // Finally regardless if the promise is resovlved or rejected