//  String primitive
const message = 
'This is my\n' +
'\'first\' message'

// Literals
// Object {}
// Boolean true, false
// String '', ""
// Template ``

const another = 
`Hi Arcy,

// Returns a value to function 
${message}

Thank you for Joining

Regards,
Aigoo`

console.log(another)