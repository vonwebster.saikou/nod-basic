// Async Iterators
// note : iterator lecture can be find in es-six/iterator.js
// same as normal iterator but returns promises

function createStore() {
    const tables = {
        customer: {
            1: { name: 'Aigoo' },
            2: { name: 'Cornietta' },
            3: { name: 'Hamtaro' },
        },
        food: {
            1: [ 'cake', 'waffle' ],
            2: [ 'coffee' ],
            3: [ 'apple', 'carrot' ],
        }
    }

    return {
        get: (table, id) => delay(1000).then(() => tables[table][id])
    }
}
function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}

const store = createStore()
console.log(store.get('customer', 1))

const customers = {
    [Symbol.iterator]: function() {
        let i = 0
        return {
            next: async function() {
                i++
                const customer = await store.get('customer', i)

                if(!customer) {
                    return { done: true }
                }

                customer.foods = await store.get('food', i)
                return {
                    value: customer,
                    done: false
                }
            }
        }
    }
}
// IIFE - Immediately-invoked function expression

;(async function() {
    const iterator = customers[Symbol.iterator]()
    console.log(await iterator.next())
    console.log(await iterator.next())
    console.log(await iterator.next())
    console.log(await iterator.next())
    console.log(await iterator.next())
    
    /*for await(const customer of customers) {
        console.log(customer)
    }*/
})()
