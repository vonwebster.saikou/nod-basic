
const birthdayParty = {
    cakes: 1,
    guests: 5,
    tables: 1,
    chairs: 5,
    drink: 'tea',
}
const { chairs, cakes, ...teaCeremony } = birthdayParty

console.log(chairs, cakes, teaCeremony)

const teaCeremonyNight = { ...teaCeremony }
teaCeremonyNight.drink = 'Chamomile Tea'
console.log(teaCeremony)
console.log(teaCeremonyNight)

const netflixEvening = { ...teaCeremony, ...{ guests: 0, netflix: true } }
console.log(netflixEvening)

const user = {
    firstName: 'Bramus',
    lastName: 'Van Damme',
    twitter: 'bramus',
    city: 'Vinkt',
    email: 'bramus@bram.us',
}
const userWithoutEmail = Object.assign({}, user);
// `delete` used to remove a property
delete userWithoutEmail.email;

console.log(user)
console.log(userWithoutEmail)

// OR SIMPLY REMOVE PROPERTY BY DESTRUCTURING
// in this example we will remove the city
// putting the `rest` of user to the userWithoutCity
const { city, ...userWithoutCity } = user
console.log(userWithoutCity)