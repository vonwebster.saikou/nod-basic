// Big O Definition
// -- We say that algorithm is  O(f(n)) if the number of simple
// operation has to do is eventually lessthan a constant times f(n)
// as n increase

// -- f(n) could be linear (f(n) = n)
// -- f(n) could be quadratic (f(n) = n^2)
// -- f(n) could be constant (f(n) = 1)
// -- f(n) could be something entirely different!

function addUpto(n) {
    return n * (n + 1) / 2;
}
console.log(addUpto(100001));
// Always 3 operations
// flat line of time of execution

function addUptoLinear(n) {
    let total = 0;
    for (let i = 0; i < n; i++) {
        total += i;
    }
    return total;
}
// linear

function printAllPairs(n) {
    for (let i = 0; i < n; i++) { // O(n)
        for (let j = 0; j < n; j++) { // O(n)
            console.log(i, j);            
        }
    }
}
// Operation inside a Operation
// O(n^2)

// O(nlog n)
// O(log n)

// When Determining the time complexity of an algorigthm, 
// there are some helpful rule of thumbs for big O expressions

// -- Constants don't Matter
// 1 -- O(2n) x ==> O(n)
// 2 -- O(500) x ==> O(1)
// 3 -- O(13n^2) x ==> O(n^2)

// Big O Shorthands
// Analyzing complexity with big O can get complicated
// There are several rules of thumb that can help
// These rules won't ALWAYS work, but are a helpful starting point

// 1 -- Arithmetic opearion are constant time
// 2 -- variable assignment in constant
// 3. Accessing elements in an array (by index) or object (by key) is constant
// 4. In a loop, the complexity is the length of the loop times 
// the complexity of what ever happens inside the loop


function logAtLeast5(n) {
    for (let i = 1; i < Math.max(5, n); i++) {
        console.log(i);        
    } // O(n)
}
function logAtLeast5(n) {
    for (let i = 1; i < Math.min(5, n); i++) {
        console.log(i);        
    } // O(1)
}

// -- Time complexity - A way of showing how the runtime 
// of a function increases
// n is the size of the in put
// 1. Linear time O(n) 
// 2. Constant time // time it takes doesn't increate as  O(1)
// the size of the input increases (When the times state is constant)
// 3. Quadratic Time - a time increases like a quadratic function O(n^2)

// T = an +b = O(n)
 // -- Find the fastest growing term [an]
 // -- Take out the coefficient which is [a]
// T = cn^2 + dn + e
 // O(n^2)